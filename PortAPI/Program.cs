using Microsoft.EntityFrameworkCore;
using PortAPI;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<PortContext>(o => o.UseSqlServer(builder.Configuration.GetConnectionString("IDS")));
var app = builder.Build();

app.MapGet("/", () => "Hello World!");

app.MapGet("/api/ports", async (PortContext db) => await db.Ports.ToListAsync());

app.MapGet("/api/ports/{id}", async (PortContext db, int id) => await db.Ports.FindAsync(id));

app.MapPost("/api/ports", async (PortContext db, Port port) =>
 {
     await db.Ports.AddAsync(port);
     await db.SaveChangesAsync();
     Results.Accepted();
 });

app.MapPut("/api/ports/{id}", async (PortContext db, int id, Port port) =>
{
    if (id != port.portid) return Results.BadRequest();

    db.Update(port);
    await db.SaveChangesAsync();

    return Results.NoContent();

});

app.MapDelete("/api/ports/{id}", async (PortContext db, int id) =>
{
    var port = await db.Ports.FindAsync(id);
    if(port == null) return Results.NotFound();

    db.Ports.Remove(port);
    await db.SaveChangesAsync();

    return Results.NoContent();
});


app.Run();
