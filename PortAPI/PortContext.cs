﻿using Microsoft.EntityFrameworkCore;

namespace PortAPI
{
    public class PortContext : DbContext
    {
        public PortContext(DbContextOptions<PortContext> options) : base(options)
        {

        }

        public DbSet<Port> Ports { get; set; }
    }

    public class Port
    {
        public int portid { get; set; }
        public string portcode { get; set; }
        public string portname{ get; set; }
        public string countrycode { get; set; }
    }
}
